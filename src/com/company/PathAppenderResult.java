package com.company;

/**
 * Created by alibenzarrouk on 21/06/2017.
 */
public class PathAppenderResult {

    private Matrix _rightMatrix;
    private Path _pathBeforeRightMatrix;
    private Matrix _downMatrix;
    private Path _pathBeforeDownMatrix;

    public PathAppenderResult(Matrix rightMatrix, Path pathBeforeRightMatrix, Matrix downMatrix, Path pathBeforeDownMatrix)
    {
        _rightMatrix = rightMatrix;
        _pathBeforeRightMatrix = pathBeforeRightMatrix;
        _downMatrix = downMatrix;
        _pathBeforeDownMatrix = pathBeforeDownMatrix;
    }

    public Matrix get_rightMatrix() {
        return _rightMatrix;
    }

    public void set_rightMatrix(Matrix _rightMatrix) {
        this._rightMatrix = _rightMatrix;
    }

    public Matrix get_downMatrix() {
        return _downMatrix;
    }

    public void set_downMatrix(Matrix _downMatrix) {
        this._downMatrix = _downMatrix;
    }

    public Path get_pathBeforeDownMatrix() {
        return _pathBeforeDownMatrix;
    }

    public void set_pathBeforeDownMatrix(Path _pathBeforeDownMatrix) {
        this._pathBeforeDownMatrix = _pathBeforeDownMatrix;
    }

    public Path get_pathBeforeRightMatrix() {
        return _pathBeforeRightMatrix;
    }

    public void set_pathBeforeRightMatrix(Path _pathBeforeRightMatrix) {
        this._pathBeforeRightMatrix = _pathBeforeRightMatrix;
    }
}
