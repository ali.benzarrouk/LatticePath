package com.company;

import java.util.List;

/**
 * Created by alibenzarrouk on 21/06/2017.
 */
public class Path {

    private List<Direction> _directions;
    private bool _isValid;

    public List<Direction> get_directions() {
        return _directions;
    }

    public void set_directions(List<Direction> _directions) {
        this._directions = _directions;
    }

    public bool get_isValid() {
        return _isValid;
    }

    public void set_isValid(boolean _isValid) {
        this._isValid = _isValid;
    }
}
