package com.company;

/**
 * Created by alibenzarrouk on 21/06/2017.
 */
public class Matrix {

    private int _columns;
    private int _rows;

    public int get_columns() {
        return _columns;
    }

    public void set_columns(int _columns) {
        this._columns = _columns;
    }

    public int get_rows() {
        return _rows;
    }

    public void set_rows(int _rows) {
        this._rows = _rows;
    }
}
