import com.company.Direction;
import com.company.Matrix;
import com.company.Path;
import com.company.PathAppenderResult;

import java.util.concurrent.Callable;

/**
 * Created by alibenzarrouk on 21/06/2017.
 */
public class PathAppender implements Callable<PathAppenderResult>{

    private Path _path;
    private Matrix _matrix;

    public PathAppender(Path path, Matrix matrix)
    {
        _path = path;
        _matrix = matrix;
    }

    private Matrix GetMatrixByDirection(Direction direction)
    {
        return null;
    }

    private void ValidatePath(Path path)
    {
        path.set_isValid(true);
    }

    private void AddDirectionTillEnd(Path path, Direction direction)
    {

    }

    @Override
    public PathAppenderResult call() throws Exception {
        Matrix rightMatrix = GetMatrixByDirection(Direction.right);
        Matrix downMatrix = GetMatrixByDirection(Direction.down);

        if (rightMatrix == null)
        {
            AddDirectionTillEnd(_path, Direction.down);
            ValidatePath(_path);
        }

        if (downMatrix == null)
        {
            AddDirectionTillEnd(_path, Direction.right);
        }

        return null;
    }
}
